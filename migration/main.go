package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/pressly/goose/v3"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"grpc_hello_world/config"
	"log"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("no. .env file provided")
	}

	conf := config.New()
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		conf.DBHost,
		conf.DBUser,
		conf.DBPassword,
		conf.DBName,
		conf.DBPort,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	// setup database

	if err := goose.SetDialect("postgres"); err != nil {
		panic(err)
	}

	currentDB, err := db.DB()
	if err != nil {
		panic(err)
	}
	var argsRaw = os.Args
	fmt.Printf("-> %#v\n", argsRaw)

	if argsRaw[1] == "up" {
		if err := goose.Up(currentDB, "migration/migrate"); err != nil {
			panic(err)
		}
	} else if argsRaw[1] == "down" {
		if err := goose.Down(currentDB, "migration/migrate"); err != nil {
			panic(err)
		}
	}

}
