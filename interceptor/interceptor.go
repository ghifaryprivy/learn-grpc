package interceptor

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
)

func ServerInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		log.Println("unable to get metadata from ctx")
	}
	authorization := md.Get("Authorization")
	if len(authorization) == 0 {
		return nil, status.New(codes.Unauthenticated, "Syeh").Err()
	}

	if authorization[0] == "" {
		return nil, status.New(codes.Unauthenticated, "Syeh").Err()
	}

	h, err := handler(ctx, req)

	return h, nil
}
