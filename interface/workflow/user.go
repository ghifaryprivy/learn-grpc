package workflow

import (
	"context"
	"fmt"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/model/user"
)

type UserService struct {
	user.UnimplementedUserServiceServer
	DB *gorm.DB
}

func (u UserService) GreetUser(ctx context.Context, request *user.GreetingRequest) (*user.GreetingResponse, error) {
	salutationMessage := fmt.Sprintf("Howdy, %s %s, nice to see you in the future!", request.Salutation, request.Name)

	return &user.GreetingResponse{GreetingMessage: salutationMessage}, nil
}

func (u UserService) AllGreeting(ctx context.Context, request *user.GreetingAllRequest) (*user.Greetings, error) {
	return &user.Greetings{
		Greetings: []*user.GreetingRequest{
			{
				Name:       "Azmi",
				Salutation: "Example",
			},
			{
				Name:       "Azmi loro",
				Salutation: "Example loro",
			},
		},
	}, nil
}

func (us *UserService) GetAllUsers(ctx context.Context, req *user.GetUserRequest) (*user.AllUserResponse, error) {
	var users []*entity.User
	if req.Key != "" {
		us.DB.Where("name = ?", req.Key).Find(&users)
	} else {
		us.DB.Find(&users)
	}

	AllUser := []*user.DetailUser{}

	for _, usr := range users {
		AllUser = append(AllUser, &user.DetailUser{
			Id:      usr.ID,
			Name:    usr.Name,
			Address: usr.Address,
		})
	}

	allUsers := &user.AllUserResponse{Users: AllUser}

	return allUsers, nil
}

func (us *UserService) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.DetailUser, error) {
	currentUser := &entity.User{
		Name:    req.Name,
		Address: req.Address,
	}
	us.DB.Create(currentUser)
	return &user.DetailUser{
		Name:    req.Name,
		Address: req.Address,
	}, nil
}

func (us *UserService) UpdateUser(ctx context.Context, req *user.UpdateUserRequest) (*user.DetailUser, error) {
	var usr entity.User
	usr.Name = req.Name
	usr.Address = req.Address
	err := usr.Validate()
	if err != nil {
		st := status.New(codes.InvalidArgument, "ada error")
		//desc := err.Error()

		v := &errdetails.BadRequest_FieldViolation{
			Field:       "id",
			Description: "iki eror",
		}
		br := &errdetails.BadRequest{}
		br.FieldViolations = append(br.FieldViolations, v)
		st, _ = st.WithDetails(br)
		return nil, st.Err()
	}
	us.DB.First(&usr, "id = ?", req.Id)
	us.DB.Model(&usr).Update("Name", req.Name)
	us.DB.Model(&usr).Update("Address", req.Address)

	return &user.DetailUser{
		Id:      req.Id,
		Name:    req.Name,
		Address: req.Address,
	}, nil
}

func (us *UserService) DeleteUser(ctx context.Context, req *user.DeleteUserRequest) (*user.DeleteUserResponse, error) {
	var usr *entity.User
	us.DB.Delete(&usr, "id = ?", req.Id)

	return &user.DeleteUserResponse{
		Message: "Successfully Deleted User",
	}, nil
}

//func (us *UserService) CreateWithValidation(ctx context.Context, req *user.CreateWithValidationRequest) (*user.DetailUser, error) {
//	err := validation.Validate(req.Name)
//}
