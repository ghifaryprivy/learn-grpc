package config

import "os"

type DBConfig struct {
	DBDriver   string
	DBHost     string
	DBPort     string
	DBUser     string
	DBName     string
	DBPassword string
	DBTimeZone string
	DBLog      bool
}

type Config struct {
	DBConfig
}

func New() *Config {
	return &Config{
		DBConfig: DBConfig{
			DBDriver:   getEnv("DB_DRIVER", "postgres"),
			DBHost:     getEnv("DB_HOST", "127.0.0.1"),
			DBPort:     getEnv("DB_PORT", "5432"),
			DBUser:     getEnv("DB_USER", "root"),
			DBName:     getEnv("DB_NAME", "go"),
			DBPassword: getEnv("DB_PASSWORD", "root"),
			DBTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	value, exists := os.LookupEnv(key)
	if exists {
		return value
	}

	nextValue := os.Getenv(key)
	if nextValue != "" {
		return nextValue
	}

	return defaultVal
}
