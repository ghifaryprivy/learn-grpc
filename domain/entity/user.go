package entity

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID      string `sql:"type:uuid;primary_key;default:uuid_generate_v4()"`
	Name    string
	Address string
}

func (user *User) BeforeCreate(tx *gorm.DB) (err error) {
	// UUID version 4
	user.ID = uuid.NewString()
	return
}

func (user User) Validate() error {
	return validation.ValidateStruct(&user,
		validation.Field(&user.Name, validation.Required, validation.Length(5, 100)),
		validation.Field(&user.Address, validation.Required, validation.Length(5, 100)),
	)
}
