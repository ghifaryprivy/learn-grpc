package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"grpc_hello_world/model/user"
	"log"
	"time"
)

const userServiceAddress = "localhost:5050"

func main() {
	conn, err := grpc.Dial(userServiceAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect to service: %v", err)
		return
	}
	defer conn.Close()
	userServiceClient := user.NewUserServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	response, err := userServiceClient.GreetUser(ctx, &user.GreetingRequest{
		Name:       "Martinah",
		Salutation: "Mister Mar",
	})

	if err != nil {
		log.Fatalf("Could not create request: %v", err)
	}
	fmt.Println(response.GreetingMessage)
}
