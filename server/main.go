package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"grpc_hello_world/config"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/interceptor"
	"grpc_hello_world/interface/workflow"
	"grpc_hello_world/model/user"
	"log"
	"net"
)

type userService struct {
	user.UnimplementedUserServiceServer
}

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Println("no. .env file provided")
	}

	conf := config.New()
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		conf.DBHost,
		conf.DBUser,
		conf.DBPassword,
		conf.DBName,
		conf.DBPort,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	db.AutoMigrate(&entity.User{})

	//Create TCP Server on localhost:5001
	lis, err := net.Listen("tcp", ":5001")
	if err != nil {
		log.Fatalf("Failed to listen on port: %v", err)
	}
	//Create new gRPC server handler
	server := grpc.NewServer(
		grpc.ChainUnaryInterceptor(interceptor.ServerInterceptor),
	)

	//register gRPC UserService to gRPC server handler
	user.RegisterUserServiceServer(server, &workflow.UserService{DB: db})

	reflection.Register(server)
	//Run server
	if err := server.Serve(lis); err != nil {
		log.Fatal(err.Error())
	}
}
